//
//  GreenViewController.swift
//  G49L6
//
//  Created by Ivan Vasilevich on 12/20/16.
//  Copyright © 2016 Ivan Besarab. All rights reserved.
//

import UIKit

class GreenViewController: UIViewController {

    @IBOutlet weak var secondSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        secondSlider.value = GlobalDB.shared.sliderValue
        // Do any additional setup after loading the view.
    }

    @IBAction func backButtonPressed() {
        _ = navigationController?.popViewController(animated: true)
    }
   
}
