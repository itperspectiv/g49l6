//
//  ViewController.swift
//  G49L6
//
//  Created by Ivan Vasilevich on 12/20/16.
//  Copyright © 2016 Ivan Besarab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var slider: UISlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
                                       green: CGFloat(arc4random_uniform(256))/255,
                                       blue: CGFloat(arc4random_uniform(256))/255,
                                       alpha: 1)
        // Do any additional setup after loading the view, typically from a nib.
        print(slider)
        slider.backgroundColor = UIColor(red: CGFloat(arc4random_uniform(256))/255,
                                         green: CGFloat(arc4random_uniform(256))/255,
                                         blue: CGFloat(arc4random_uniform(256))/255,
                                         alpha: 1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        GlobalDB.shared.sliderValue = slider.value
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        segue.destination.title = "green"
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    



}

